import React from 'react'
import  { useState } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/js/dist/modal';
import 'bootstrap/dist/js/bootstrap.bundle';
const Contact = () =>  {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(!re.test(email)){
        setEmail("Invalid mail");
    }
  };

  return (
    <div class="container3">
        <div class="container4">
    <form onSubmit={handleSubmit}>
      <label>Name:
        <input type="text"  value={name} onChange={(e) => setName(e.target.value)}/>
      </label><br/>
      <label>Email:
        <input type="text"  value={email} onChange={(e) => setEmail(e.target.value)}/>
      </label><br/>
      <label>Message:
        <textarea value={message}  onChange={(e) => setMessage(e.target.value)}/>
      </label><br/>
      <button type="submit" class="button11 justify-content-center">Submit</button>
    </form>
    </div>
    </div>
  )
};
export default Contact;