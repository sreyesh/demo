import React from 'react'

const About=() =>{
    return(
        <div class="About">
            <center>
               <p class="Sheading">Bolt Abacus Private Limited (Group Company) the pioneers in the field of Abacus education in India, since 1999. Bolt Abacus company has launched its newly invented state-of-the-art Bolt Abacus Digital and Bolt Abacus Non-digital both for students and tutors and the program to make the children of the age group – 5 to 13 years, benefits much more than ever before. Bolt Abacus products and the program structured training Material are the results of 14 years of background research.</p>
            </center>
        </div>
    )
}
export default About